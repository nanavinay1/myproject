package CustomExceptionPackage;

import java.util.Scanner;

public class Class1
{

	public static void method(String s) throws CustomExceptionCreation
	{
		System.out.println(s);
		if(s.equalsIgnoreCase("CustomException"))
		{
			System.out.println("String Matched");
		}
		else
		{
		throw new CustomExceptionCreation(s+" String Not Matched");
		}
		
	}
	public static void main(String[] args)
	{
		try
		{
			Scanner sc = new Scanner(System.in);
			String s = sc.nextLine();
			method(s);
		}
		catch(CustomExceptionCreation ce)
		{
			System.out.println("Error is "+ce);
			ce.printStackTrace();
		}
	}
}
