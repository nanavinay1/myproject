package TryResource;

import java.util.Scanner;
import TryResource.TryResourcePojo;


public class TryResource 
{
	
	public static void main(String[] args)
	{
		try(TryResourcePojo trp = new TryResourcePojo())
		{
			trp.methodd();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
}
